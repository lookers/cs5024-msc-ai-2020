# CS5024 - Theory & Practice of Advanced AI Ecosystems

## Project 1

**Student Name:** TENG YU

**Student ID:** 18195253

**Student Email:** 18195253@studentmail.ul.ie

---
Instructions
Your next task is to build a Chain Reaction AI using the DSL provided in DIME.
In DIME, these are main steps you will likely need:

Add your First SIB
Add a new data context
Add a corner Check
Calculate the final score of your game
Play against your own AI – Can you beat it?
Group a feature of the AI into a reusable SIB (Hint – use Hierarchy for the Sub-AI Feature)
Ensure the neatness and readability of the model (Hint – appropriate use of Hierarchy and layout)

