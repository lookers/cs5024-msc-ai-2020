# CS5024 - Theory & Practice of Advanced AI Ecosystems

## Project 1

**Student Name:** Ruiwu Qiu

**Student ID:** 18200753

**Student Email:** 18200753@studentmail.ul.ie

---
Deliverables:

| Filename               | Description                                          |
|------------------------|------------------------------------------------------|
| RickAI.process         | A top level Chain Reaction AI model                  |
| IsCornerOrEdge.process | A Sub AI works as a SIB for the top level AI         |
 